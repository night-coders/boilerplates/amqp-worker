import * as envs from '@/application/config/environment'

describe('Application > Config > Environment', () => {
  it('Expect all environment variables to be set and no one be forgotten', () => {
    const {
      AMQP_HOST,
      ...variables
    } = envs
    expect(AMQP_HOST).not.toBeUndefined()
    expect(variables).toEqual({})
  })

  it('Expect all environments to be override from process.env', () => {
    process.env = {
      AMQP_HOST: 'amqp://only-for-tests',
    }

    jest.resetModules()

    const {
      AMQP_HOST,
    } = require('@/application/config/environment')

    expect(AMQP_HOST).toEqual('amqp://only-for-tests')
  })
})

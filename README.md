# AMQP worker boilerplate

Boilerplate to start an AMQP worker easily using typescript and clear architecture

<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=6 orderedList=false} -->

## Dependencies

- [Docker + Docker Compose](https://docs.docker.com/get-docker/)
- [Node JS](https://nodejs.org/en/download/)

## Running

1. Install dependencies: `yarn`
2. Copy the file `.env.example` to `.env`
3. Start container: `docker compose up -d`

> **_<!>_** And happy coding!

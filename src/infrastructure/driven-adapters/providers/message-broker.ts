import { BrokerConfig } from '@/application/config'
import { IConsumer } from '@/infrastructure/services'

export interface IMessageBroker {
  readonly provider: string
  initialize: (config: BrokerConfig, consumer: IConsumer) => Promise<void>
  consume: (consumer: IConsumer) => Promise<void>
}
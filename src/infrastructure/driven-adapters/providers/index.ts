import { IMessageBroker } from '@/infrastructure/driven-adapters/providers/message-broker'

export type Provider = {
  provider: string
  class: IMessageBroker
}
export const providers: Provider[] = []

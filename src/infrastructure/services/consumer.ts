import MessageModel from '@/domain/models/message'

export type MessageHandler = (
  message: MessageModel,
  content: Buffer,
  ackHandler: (error?: Error) => void
) => void

export interface IConsumer {
  readonly provider: string
  readonly exchange: string
  readonly queue: string
  readonly key?: string

  messageHandler: MessageHandler
}
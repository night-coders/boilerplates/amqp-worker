import { IConsumer } from '@/infrastructure/services/consumer'

const consumers: IConsumer[] = []

export { consumers, IConsumer }
import { providers, Provider } from '@/infrastructure/driven-adapters/providers'
import { consumers as serviceConsumers, IConsumer } from '@/infrastructure/services'
import { brokers, BrokerConfig } from '@/application/config'

export default class App {
  constructor (
    private readonly consumers: IConsumer[] = serviceConsumers,
  ) {}

  async initialize() {
    this.consumers.forEach(
      async (consumer) => {
        const { class: messageBroker} = providers.find(
          ({ provider }: Provider) => provider === consumer.provider
        )
        const brokerConfig = brokers.find(
          ({ provider }: BrokerConfig) => provider === consumer.provider
        )

        await messageBroker.initialize(brokerConfig, consumer)
        messageBroker.consume(consumer)
      }
    )
  }
}
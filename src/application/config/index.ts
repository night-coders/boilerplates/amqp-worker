import { AMQP_HOST } from '@/application/config/environment'

export type BrokerConfig = {
  provider: string
  group: string
  url: string
}

export const brokers: BrokerConfig[] = [
  {
    provider: 'example-provider',
    group: '/',
    url: AMQP_HOST,
  }
]
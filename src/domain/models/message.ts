interface MessageFields {
  deliveryTag: number;
  redelivered: boolean;
  exchange: string;
  routingKey: string;
  messageCount?: number | undefined;
  consumerTag?: string | undefined;
}

interface MessageProperties {
  contentType: any | undefined;
  contentEncoding: any | undefined;
  headers: MessagePropertyHeaders;
  deliveryMode: any | undefined;
  priority: any | undefined;
  correlationId: any | undefined;
  replyTo: any | undefined;
  expiration: any | undefined;
  messageId: any | undefined;
  timestamp: any | undefined;
  type: any | undefined;
  userId: any | undefined;
  appId: any | undefined;
  clusterId: any | undefined;
}

interface MessagePropertyHeaders {
  "x-first-death-exchange"?: string | undefined;
  "x-first-death-queue"?: string | undefined;
  "x-first-death-reason"?: string | undefined;
  "x-death"?: XDeath[] | undefined;
  [key: string]: any;
}

interface XDeath {
  count: number;
  reason: "rejected" | "expired" | "maxlen";
  queue: string;
  time: {
      "!": "timestamp";
      value: number;
  };
  exchange: string;
  "original-expiration"?: any;
  "routing-keys": string[];
}

export default class MessageModel {
  fields: MessageFields

  properties: MessageProperties

  content: Buffer

  constructor({ fields, properties, content }: any) {
    this.fields = this.setFields(fields)
    this.properties = this.setProperties(properties)
    this.content = content
  }

  private setFields({
    deliveryTag,
    redelivered,
    exchange,
    routingKey,
    messageCount,
    consumerTag,
  }: MessageFields) {
    return {
      deliveryTag,
      redelivered,
      exchange,
      routingKey,
      messageCount,
      consumerTag,
    }
  }

  private setProperties({
    contentType,
    contentEncoding,
    headers,
    deliveryMode,
    priority,
    correlationId,
    replyTo,
    expiration,
    messageId,
    timestamp,
    type,
    userId,
    appId,
    clusterId,
  }: MessageProperties) {
    return {
      contentType,
      contentEncoding,
      headers: this.setHeaders(headers),
      deliveryMode,
      priority,
      correlationId,
      replyTo,
      expiration,
      messageId,
      timestamp,
      type,
      userId,
      appId,
      clusterId,
    }
  }

  private setHeaders(headers: MessagePropertyHeaders) {
    return Object.keys(headers).reduce(
      (headerList: MessagePropertyHeaders, key: string ) => ({...headerList, [key]: headers[key]}),
      {}
    )
  }
}
/*
 * For a detailed explanation regarding each configuration property and type check, visit:
 * https://jestjs.io/docs/configuration
 */

export default {
  clearMocks: true,
  collectCoverage: false,
  collectCoverageFrom: [
    "**/src/**/*.[jt]s?(x)"
  ],
  coverageDirectory: "coverage",
  coveragePathIgnorePatterns: [
    "/node_modules/",
    "/src/domain/models/",
    "/src/index.ts",
    "/src/application/app.ts"
  ],
  coverageProvider: "v8",
  moduleNameMapper: {
    "@/(.*)": "<rootDir>/src/$1"
  },
  modulePathIgnorePatterns: [
    "<rootDir>/dist",
    "<rootDir>/data",
    "<rootDir>/coverage",
    "<rootDir>/docs"
  ],
  preset: 'ts-jest',
  rootDir: './',
  roots: [
    "<rootDir>"
  ],
  testEnvironment: "node",
  testMatch: [
    "**/tests/**/*.[jt]s?(x)"
  ],
  testResultsProcessor: "jest-sonar-reporter",
  transform: {
    "^.+\\.(ts)?$": "ts-jest"
  },
  transformIgnorePatterns: [
    "<rootDir>/node_modules/"
  ],
  passWithNoTests: false
};

FROM node:14.4.0-alpine

WORKDIR /app
COPY . .

CMD ["node", "-r", "module-alias/register", "/app/dist/index.js"]